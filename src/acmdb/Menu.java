package acmdb;

import java.lang.*;
import java.sql.*;
import java.io.*;
import java.util.*;

public class Menu {
    public static Scanner scanner;
    public static Connector con;
	public static void topbooks(Timestamp cur){
		try{
            System.out.println("top:");
            int top = Integer.parseInt(scanner.nextLine());
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct b.isbn, b.title, sum(p.copyno) as sold from Book b, Buy p where p.bdate >= '%s' and b.isbn = p.isbn group by p.isbn order by sum(p.copyno) desc", cur.toString()));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                System.out.println("isbn: " + rs.getString("isbn") + ", " + "title: " + rs.getString("title") + ", " + "sold: " + rs.getString("sold")); 
				top --;
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}
	}
	public static void toppublishers(Timestamp cur){
		try{
            System.out.println("top:");
            int top = Integer.parseInt(scanner.nextLine());
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct b.publisher, sum(p.copyno) as sold from Book b, Buy p where p.bdate >= '%s' and b.isbn = p.isbn group by b.publisher order by sum(p.copyno) desc", cur.toString()));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                System.out.println("publisher: " + rs.getString("publisher") + ", " + "sold: " + rs.getString("sold"));
				top --;
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}
	}
	public static void topauthors(Timestamp cur){
		try{
            System.out.println("top:");
            int top = Integer.parseInt(scanner.nextLine());
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct w.author, sum(p.copyno) as sold from Writes w, Buy p, Author a where p.bdate >= '%s' and w.isbn = p.isbn and w.author = a.name group by a.name order by sum(p.copyno) desc", cur.toString()));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                System.out.println("author: " + rs.getString("author") + ", " + "sold: " + rs.getString("sold"));
				top --;
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}
	}

    public static void statistics() {
		Timestamp cur, after;
		java.util.Date date = new java.util.Date();
		try{
			ResultSet current = con.stmt.executeQuery("select now()");
			current.next();
			cur = current.getTimestamp(1);
			//System.out.println(cur.toString());
			long ms = cur.getTime();
			ms -= 1000. * 60. * 60. * 24. * 30. * 3.;
			after = new Timestamp(ms);

/*		try{
		}catch (Exception e){
			System.out.println("Time Converting Error");
		}*/

		while (true) {
			System.out.println("+------------------------------+");
			System.out.println("|           STATISTICS         |");
			System.out.println("|1. Popular books              |");
			System.out.println("|2. Popular authors            |");
			System.out.println("|3. Popular publishers         |");
			System.out.println("|4. Go back                    |");
			System.out.println("+------------------------------+");
			String c = scanner.nextLine();
			if (c.equals("1")) {
				topbooks(after);
			}
			else if (c.equals("2")) {
				topauthors(after);
			}
			else if (c.equals("3")) {
				toppublishers(after);
			}
			else if (c.equals("4")) {
				break;
			}
			else {
				System.out.println("Invalid Input");
			}
		}
		}catch (Exception e){
			System.out.println("Time Getting Error");
			System.out.println(e.getCause());
		}

    }
	public static void mostTrusted(){
		try{
            System.out.println("top:");
            int top = Integer.parseInt(scanner.nextLine());
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct ratee, sum(trust) as trustness from RateUser group by ratee order by sum(trust) desc"));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                System.out.println("user: " + rs.getString("ratee") + ", " + "trustscore: " + rs.getString("trustness"));
				top --;
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}

	}
	public static void mostUseful(){
		try{
            System.out.println("top:");
            int top = Integer.parseInt(scanner.nextLine());
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct reviewer, avg(score) as usefulness from RateFeedback group by reviewer order by avg(score) desc"));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                System.out.println("user: " + rs.getString("reviewer") + ", " + "score: " + rs.getString("usefulness"));
				top --;
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}

	}
    public static void userAwards() {
		while(true){
			System.out.println("+------------------------------+");
			System.out.println("|        USER AWARDS           |");
			System.out.println("|1. Most trusted award         |");
			System.out.println("|2. Most useful award          |");
			System.out.println("|3. Go back                    |");
			System.out.println("+------------------------------+");
			String c = scanner.nextLine();
			if (c.equals("1")) {
				mostTrusted();
			}
			else if (c.equals("2")) {
				mostUseful();
			}
			else if (c.equals("3")) {
				break;
			}
			else {
				System.out.println("Invalid Input");
			}
		}
    }
    public static void browseBooks(String logname) {
        try {
            String authors, publisher, title_words, subject;
            System.out.println("authors(seperated by comma, no additional space, if none, enter return):");
            authors = scanner.nextLine();
            System.out.println("publisher(if none, enter return):");
            publisher = scanner.nextLine();
            System.out.println("title-words(seperated by comma, no additional space, if none, enter return):");
            title_words = scanner.nextLine();
			System.out.println("subject(if none, enter return):");
            subject = scanner.nextLine();
		
			String[] author = authors.split(",");        
			String[] title = title_words.split(",");        


			String select = String.format("select distinct b.isbn, title, b.author, publisher ");   
			String from = "from Book b "; 
			String where = "";
			String sort = "";
			String group = "";
			boolean begin = true;

            if (!(author.length == 1 && author[0].equals(""))) {
                for (int i = 0; i < author.length; i ++){
                    if (!begin) where += "and "; else {begin = false;where += "where ";}
                    where += String.format(" exists(select * from Writes w where b.isbn = w.isbn and w.author like '%%%s%%') ", author[i]);
                }
            }

			if (!publisher.isEmpty()){
				if (!begin) where += "and "; else {begin = false;where += "where ";}
				where += String.format("b.publisher like '%%%s%%' ", publisher);
			}

			if (!subject.isEmpty()){
				if (!begin) where += "and "; else {begin = false;where += "where ";}
				where += String.format("b.subject like '%%%s%%' ", subject);
			}

            if (!(title.length == 1 && title[0].equals(""))) {
                for (int i = 0; i < title.length; i ++){
                    if (!begin) where += "and "; else {begin = false;where += "where ";}
                    where += String.format("b.title like '%%%s%%' ", title[i]);
                }
            }
            System.out.println("+--------------------------------------+");
            System.out.println("|               SORT BY                |");
            System.out.println("|1. Publishing year                    |");
            System.out.println("|2. Average feedback                   |");
            System.out.println("|3. Average feedback from trusted user |");
            System.out.println("+--------------------------------------+");
            String c = scanner.nextLine();
            if (c.equals("1")) {
				select += ", pyear ";
				sort = "order by b.pyear";
				ResultSet rs = con.stmt.executeQuery(select + from + where + sort);
				System.out.println("##############################");
				while (rs.next()) {
					System.out.println("isbn: " + rs.getString("isbn") + ", " + "title: " + rs.getString("title") + ", authors: " + rs.getString("author") + ", publisher: " + rs.getString("publisher") + ", publish year:" + rs.getString("pyear"));
				}
				System.out.println("##############################");
            }
            else if (c.equals("2")) {
				select += ", avg(f.score) as feed ";
				from += ", Feedback f ";
                if (!begin) where += "and "; else {begin = false;where += "where ";}
				where += " f.isbn = b.isbn ";
				group = "group by f.isbn ";
				sort += "order by avg(f.score) ";
				ResultSet rs = con.stmt.executeQuery(select + from + where + group + sort); 
				System.out.println("##############################");
				while (rs.next()) {
					System.out.println("isbn: " + rs.getString("isbn") + ", " + "title: " + rs.getString("title") + ", authors: " + rs.getString("author") + ", publisher: " + rs.getString("publisher") + ", average feedback:" + rs.getString("feed"));
				}
				System.out.println("##############################");

            }
            else if (c.equals("3")) {
				select += ", avg(f.score) as feed ";
				from += ", Feedback f ";
                if (!begin) where += "and "; else {begin = false;where += "where ";}
				where += String.format(" f.isbn = b.isbn and exists(select * from RateUser r where r.rater = '%s' and r.ratee = f.logname and r.trust = 1) ", logname);
				group = "group by isbn "; 
				sort += "order by avg(f.score) ";
				ResultSet rs = con.stmt.executeQuery(select + from + where + group + sort); 
				System.out.println("##############################");
				while (rs.next()) {
					System.out.println("isbn: " + rs.getString("isbn") + ", " + "title: " + rs.getString("title") + ", authors: " + rs.getString("author") + ", publisher: " + rs.getString("publisher") + ", average feedback from trusted users:" + rs.getString("feed"));
				}
				System.out.println("##############################");

            }
            else {
                System.out.println("Invalid Input");
            }
        }
        catch (Exception e) {
            System.out.println("Invalid Operation");
        }
    }
    public static void suggest(String id, String isbn) {
		try{
			ResultSet rs = con.stmt.executeQuery(String.format("select distinct(isbn), sum(copyno) as cpy from Buy where logname in (select logname from Buy b where b.isbn = '%s') and logname <> '%s' and isbn <> '%s' group by isbn order by sum(copyno) desc", isbn, id, isbn));
            System.out.println("##############################");
            System.out.println("People Also Buy:");
            while (rs.next()) {
                System.out.println("book: " + rs.getString("isbn") + ", " + "sales: " + rs.getString("cpy"));
            }
            System.out.println("##############################");
		}catch (Exception e){
            System.out.println("Invalid Operation");
		}
    }
    public static void usefulFeedback() {
        try {
            String isbn;
            int top;
            System.out.println("isbn:");
            isbn = scanner.nextLine();
            System.out.println("top:");
            top = Integer.parseInt(scanner.nextLine());
            ResultSet rs = con.stmt.executeQuery(String.format("select distinct reviewer, avg(score) as userfulness from RateFeedback where isbn = '%s' group by reviewer, isbn order by avg(score) DESC", isbn));
            System.out.println("##############################");
            while (rs.next() && top > 0) {
                top--;
                System.out.println("reviewer: " + rs.getString("reviewer") + ", " + "userfulness: " + rs.getString("userfulness"));
            }
            System.out.println("##############################");
        }
        catch (Exception e) {
            System.out.println("Invalid Operation");
        }
    }
    public static void rateUser(String id) {
        while (true) {
            String ratee, tru;
            try {
                System.out.println("user:");
                ratee = scanner.nextLine();
                if (ratee.equals(id)) {
                    System.out.println("You cannot rate yourself");
                }
                else if (con.stmt.executeQuery(String.format("select * from RateUser where rater = '%s' and ratee = '%s'",id, ratee)).next()) {
                    System.out.println("You have rated this user before");
                }
                else {
                    System.out.println("trusted/not-trusted ?");
                    tru = scanner.nextLine();
                    if (tru.equals("trusted")) {
                        con.stmt.executeUpdate(String.format("insert into RateUser (rater, ratee, trust) values ('%s', '%s', 1)", id, ratee));
                        break;
                    }
                    else if (tru.equals("not-trusted")) {
                        con.stmt.executeUpdate(String.format("insert into RateUser (rater, ratee, trust) values ('%s', '%s', -1)", id, ratee));
                        break;
                    }
                    else {
                        System.out.println("Invalid Input");
                    }
                }
            }
            catch (Exception e) {
                System.out.println("Invalid Operation");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void rateFeedback(String id) {
        while (true) {
            String reviewer, isbn;
            int score;
            try {
                System.out.println("reviewer:");
                reviewer = scanner.nextLine();
                if (reviewer.equals(id)) {
                    System.out.println("You cannot rate a review of yourselves");
                }
                else {
                    System.out.println("isbn:");
                    isbn = scanner.nextLine();
                    if (con.stmt.executeQuery(String.format("select * from RateFeedback where rater = '%s' and reviewer = '%s' and isbn = '%s'", id, reviewer, isbn)).next()) {
                        System.out.println("You cannot rate a review twice");
                    }
                    else {
                        if (!con.stmt.executeQuery(String.format("select * from Feedback where logname = '%s' and isbn = '%s'", reviewer, isbn)).next()) {
                            System.out.println("There is no such a feedback");
                        }
                        else {
                            System.out.println("score:");
                            score = Integer.parseInt(scanner.nextLine());
                            if (score < 0 || score > 2) {
                                throw new Exception();
                            }
                            con.stmt.executeUpdate(String.format("insert into RateFeedback (rater, reviewer, isbn, score) values ('%s', '%s', '%s', %d)", id, reviewer, isbn, score));
                            break;
                        }
                    }
                }
            }
            catch (Exception e) {
                System.out.println("Invalid Operation");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void browseFeedback() {
        try {
            String isbn;
            System.out.println("isbn:");
            isbn = scanner.nextLine();
            ResultSet rs = con.stmt.executeQuery(String.format("select * from Feedback where isbn = '%s'", isbn));
            System.out.println("##############################");
            while (rs.next()) {
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                System.out.println(rs.getString("logname") + " " + rs.getString("score") + " " + sdf.format(rs.getTimestamp("rdate")));
                System.out.println(rs.getString("review"));
            }
            System.out.println("##############################");
        }
        catch (Exception e) {
            System.out.println("Invalid Operation");
        }
    }
    public static void feedback(String id) {
        while (true) {
            String isbn, text;
            int score;
            try {
                System.out.println("isbn:");
                isbn = scanner.nextLine();
                System.out.println("score:");
                score = Integer.parseInt(scanner.nextLine());
                if (score < 1 || score > 10) {
                    throw new Exception();
                }
                System.out.println("text:");
                text = scanner.nextLine();
                if (con.stmt.executeQuery(String.format("select * from Feedback where isbn = '%s' and logname = '%s'", isbn, id)).next()) {
                    System.out.println("You cannot write feedback for a book twice");
                }
                else {
                    con.stmt.executeUpdate(String.format("insert into Feedback (logname, isbn, score, review) values ('%s', '%s', %d, '%s')", id, isbn, score, text));
                    break;
                }
            }
            catch (Exception e) {
                System.out.println("Invalid Operation");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }

    }
    public static void degree() {
        while (true) {
            ResultSet rs;
            String author1;
            String author2;
            try {
                System.out.println("author1:");
                author1 = scanner.nextLine();
                System.out.println("author2:");
                author2 = scanner.nextLine();
                if (con.stmt.executeQuery(String.format("select * from AuthorPair where author1 = '%s' and author2 = '%s'", author1, author2)).next()) {
                    System.out.println("1-degree");
                }
                if (con.stmt.executeQuery(String.format("select * from AuthorPair A1, AuthorPair A2 where A1.author1 = '%s' && A1.author2 = A2.author1 && A2.author2 = '%s'", author1, author2)).next()) {
                    System.out.println("2-degree");
                }
                else {
                    System.out.println("Neither 1-degree nor 2-degree");
                }
                break;
            }
            catch (Exception e) {
                System.out.println("Invalid Query");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void addPair(String a, String b) {
		try {
			if (!con.stmt.executeQuery(String.format("select * from AuthorPair where author1 = '%s' and author2 = '%s'", a, b)).next()) {
				con.stmt.executeUpdate(String.format("insert into AuthorPair (author1, author2) values ('%s', '%s')", a, b));
			}
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
	public static void addWriter(String a) {
		try {
			if (!con.stmt.executeQuery(String.format("select * from Author where name = '%s'", a)).next()) {
				con.stmt.executeUpdate(String.format("insert into Author (name) values ('%s')", a));
			}
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
	public static void addWrite(String isbn, String a) {
		try {
			if (!con.stmt.executeQuery(String.format("select * from Writes where author = '%s' and isbn = '%s'", a, isbn)).next()) {
				con.stmt.executeUpdate(String.format("insert into Writes (author, isbn) values ('%s', '%s')", a, isbn));
			}
		} catch (Exception e) {
            e.printStackTrace();
        }
	}
	public static void addWrites(String isbn, String authors) {
		String[] res = authors.split(",");        
		for (int i = 0; i < res.length; ++i){
			addWriter(res[i]);
			addWrite(isbn, res[i]);
		}
	}
    public static void addAuthorRelationship(String authors) {
        String[] res = authors.split(",");        
        for (int i = 0; i < res.length; ++i) {
            for (int j = 0; j < res.length; ++j) {
                if (i != j) {
                    addPair(res[i], res[j]);
                }
            }
        }
    }
    public static void order(String id) {
        while (true) {
            try {
                int tot = 0;
                ResultSet rs;
                String isbn;
                int cnt;
                System.out.println("isbn:");
                isbn = scanner.nextLine();
                if (con.stmt.executeQuery(String.format("select * from Book where isbn = '%s'", isbn)).next()) {
                    System.out.println("number of copies:");
                    cnt = Integer.parseInt(scanner.nextLine());
                    rs = con.stmt.executeQuery(String.format("select copyno from Book where isbn = '%s'", isbn));
                    rs.next();
                    int cpn = Integer.parseInt(rs.getString("copyno"));
                    if (cpn >= cnt) {
                        con.stmt.executeUpdate(String.format("update Book set copyno = copyno - %d where isbn = '%s'", cnt, isbn));

                        con.stmt.executeUpdate(String.format("insert into Buy (logname, isbn, copyno, bdate) values ('%s', '%s', %d, now())", id, isbn, cnt));
                        tot += cnt;
                        suggest(id, isbn);
                    }
                    else {
                        if (cpn <= 1) {
                            System.out.println(String.format("There is only %d copy left", cpn));
                        }
                        else {
                            System.out.println(String.format("There is only %d copies left", cpn));
                        }
                    }
                }
                else {
                    System.out.println("This book does not exist");
                }
                System.out.println("Continue? (Y/N)");
                String ans = scanner.nextLine();
                if (!ans.equals("Y")) {
                    System.out.println(String.format("You have ordered %d books", tot));
                    break;
                }
            }
            catch (Exception e) {
                System.out.println("Invalid Operation");
            }
        }
    }
    public static void newCopy() {
        while (true) {
            try {
                String isbn;
                int cnt;
                System.out.println("isbn:");
                isbn = scanner.nextLine();
                if (con.stmt.executeQuery(String.format("select * from Book where isbn = '%s'", isbn)).next()) {
                    System.out.println("add counts:");
                    cnt = Integer.parseInt(scanner.nextLine());
                    if (cnt <= 0) {
                        throw new Exception();
                    }
                    con.stmt.executeUpdate(String.format("update Book set copyno = copyno + %d where isbn = '%s'", cnt, isbn));
                    break;
                }
                else {
                    System.out.println("This book does not exist");
                }
            }
            catch (Exception e) {
                System.out.println("Invalid Information");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }

    }
    public static void newBook() {
        String isbn, title, author, publisher, format, keywords, subject;
        int pyear, copyno;
        float price;
        while (true) {
            try {
                System.out.println("isbn:");
                isbn = scanner.nextLine();
                System.out.println("title:");
                title = scanner.nextLine();
                System.out.println("author(s) (separated by comma, no additional space)");
                author = scanner.nextLine();
                System.out.println("publisher:");
                publisher = scanner.nextLine();
                System.out.println("format:");
                format = scanner.nextLine();
                System.out.println("keywords:");
                keywords = scanner.nextLine();
                System.out.println("subject:");
                subject = scanner.nextLine();
                System.out.println("publish year:");
                pyear = Integer.parseInt(scanner.nextLine());
                System.out.println("number of copies:");
                copyno = Integer.parseInt(scanner.nextLine());
                System.out.println("price:");
                price = Float.parseFloat(scanner.nextLine());
                con.stmt.executeUpdate(String.format("insert into Book (isbn, title, author, publisher, pyear, copyno, price, format, keywords, subject) values ('%s', '%s', '%s', '%s', %d, %d, %f, '%s', '%s', '%s')", isbn, title, author, publisher, pyear, copyno, price, format, keywords, subject));
				addWrites(isbn, author);
                addAuthorRelationship(author);
                break;
            }
            catch (Exception e) {
                System.out.println("Duplicated ISBN or Invalid Information");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void admin() throws Exception {
        while (true) {
            System.out.println("+------------------------------+");
            System.out.println("|        MANAGER CONSOLE       |");
            System.out.println("|1. Add new book               |");
            System.out.println("|2. Add new copies             |");
            System.out.println("|3. Statistics                 |");
            System.out.println("|4. User awards                |");
            System.out.println("|5. Go back                    |");
            System.out.println("+------------------------------+");
            String c = scanner.nextLine();
            if (c.equals("1")) {
                newBook();
            }
            else if (c.equals("2")) {
                newCopy();
            }
            else if (c.equals("3")) {
                statistics();
            }
            else if (c.equals("4")) {
                userAwards();
            }
            else if (c.equals("5")) {
                break;
            }
            else {
                System.out.println("Invalid Input");
            }
        }
    }
    public static void user(String id) throws Exception {
        while (true) {
            System.out.println("+----------------------------+");
            System.out.println("|        USER CONSOLE        |");
            System.out.println("|1. Order                    |");
            System.out.println("|2. Query degree             |");
            System.out.println("|3. Write a Feedback         |");
            System.out.println("|4. Browse Feedbacks         |");
            System.out.println("|5. Rate a Feedback          |");
            System.out.println("|6. Rate a User              |");
            System.out.println("|7. Useful Feedbacks         |");
            System.out.println("|8. Browse Books             |");
            System.out.println("|9. Go back                  |");
            System.out.println("+----------------------------+");
            String c = scanner.nextLine();
            if (c.equals("1")) {
                order(id);
            }
            else if (c.equals("2")) {
                degree();
            }
            else if (c.equals("3")) {
                feedback(id);
            }
            else if (c.equals("4")) {
                browseFeedback();
            }
            else if (c.equals("5")) {
                rateFeedback(id);
            }
            else if (c.equals("6")) {
                rateUser(id);
            }
            else if (c.equals("7")) {
                usefulFeedback();
            }
            else if (c.equals("8")) {
                browseBooks(id);
            }
            else if (c.equals("9")) {
                break;
            }
            else {
                System.out.println("Invalid Input");
            }
        }
    }
    public static void login() throws Exception {
        while (true) {
            System.out.println("username:");
            String username = scanner.nextLine();
            System.out.println("password:");
            String password = scanner.nextLine();
            if (username.equals("admin") && password.equals("123456")) {
                admin();
                break;
            }
            else {
                if (con.stmt.executeQuery(String.format("select * from Customer where logname = '%s' and pw = '%s';", username, password)).next()) {
                    user(username);
                    break;
                }
                else {
                    System.out.println("User does not exist or wrong password");
                }
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void registration() throws Exception {
        String username, password, fullName, address, phno;
        while (true) {
            try {
                System.out.println("username:");
                username = scanner.nextLine();
                System.out.println("password:");
                password = scanner.nextLine();
                System.out.println("full name:");
                fullName = scanner.nextLine();
                System.out.println("address:");
                address = scanner.nextLine();
                System.out.println("phone number:");
                phno = scanner.nextLine();
                con.stmt.executeUpdate(String.format("insert into Customer (cname, logname, pw, address, phno) values ('%s', '%s', '%s', '%s', '%s')", fullName, username, password, address, phno));
                break;
            } catch (Exception e) {
                System.out.println("Duplicated Username or Invalid Information");
            }
            System.out.println("Try again? (Y/N)");
            if (!scanner.nextLine().equals("Y")) {
                break;
            }
        }
    }
    public static void loginMenu() throws Exception {
        while (true) {
            System.out.println("+------------------------------+");
            System.out.println("|   WELCOME TO OUR BOOKSTORE   |");
            System.out.println("|1. Log in                     |");
            System.out.println("|2. Registration               |");
            System.out.println("|3. Exit                       |");
            System.out.println("+------------------------------+");
            System.out.println("pleasse enter your choice:");
            String c = scanner.nextLine();        
            if (c.equals("1")) {
                login(); 
            }
            else if (c.equals("2")) {
                registration();
            }
            else if (c.equals("3")) {
                System.out.println("Bye!");
                con.stmt.close(); 
                break;
            }
            else {
                System.out.println("Invalid Input");
            }
        }
    }

    public static void main(String[] args) {
        con=null;
        scanner = new Scanner(System.in);
        try {
            con= new Connector();
            loginMenu();
            con.closeConnection();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
