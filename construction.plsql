--create table Customer(cname varchar(255), logname varchar(255), pw varchar(255), address varchar(255), phno varchar(255), primary key(logname));

--create table Book(isbn varchar(255), title varchar(255), authors varchar(255), publisher varchar(255), pyear int, copyno int, price float, format char(10), keywords varchar(255), subject varchar(255), primary key (isbn));

--create table Author(name varchar(255), primary key (name));

--create table Writes(author varchar(255), isbn varchar(255), primary key (author, isbn), foreign key (author) references Author(name), foreign key (isbn) references Book(isbn));

--create table AuthorPair(author1 varchar(255), author2 varchar(255), primary key (author1, author2), foreign key (author1) references Author(name), foreign key (author2) references Author(name));

--create table Feedback(logname varchar(255), isbn varchar(255), score int, review varchar(255), rdate timestamp, primary key (logname, isbn), foreign key (logname) references Customer(logname), foreign key (isbn) references Book(isbn));

--create table Buy(logname varchar(255), isbn varchar(255), bdate timestamp, copyno int, primary key (logname, isbn, bdate), foreign key (logname) references Customer(logname), foreign key (isbn) references Book(isbn));

--create table RateFeedback(rater varchar(255), reviewer varchar(255), isbn varchar(255), score int, primary key (rater, reviewer, isbn), foreign key (rater) references Customer(logname), foreign key (reviewer) references Customer(logname), foreign key (isbn) references Book(isbn));

--create table RateUser(rater varchar(255), ratee varchar(255), trust int, primary key (rater, ratee), foreign key (rater) references Customer(logname), foreign key (ratee) references Customer(logname)); 
